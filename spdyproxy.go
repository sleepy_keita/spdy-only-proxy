package main;

import (
  "github.com/keichan34/spdy" // Import SPDY.
  "net/http"
  "net/http/httputil"
  "net/url"
  "strings"
  "fmt"
  "flag"
  "log"
)

var (
  targetHost string
  listenAddr string
)

func main() {
  var (
    certPath string
    keyPath string
  )

  flag.StringVar(&targetHost, "host", "", "Backend server.")
  flag.StringVar(&listenAddr, "listen", ":5005", "Listen address and port.")

  flag.StringVar(&certPath, "cert", "cert.pem", "Path to the SSL certificate.")
  flag.StringVar(&keyPath, "key", "key.pem", "Path to the key the certificate was signed with.")

  flag.Parse()

  if len(targetHost) == 0 {
    log.Fatal("You must specifiy the backend server.")
  }

  targetURL, err := url.Parse(targetHost)
  if err != nil {
    log.Fatal(err)
  }

  fmt.Println("Starting SPDY proxy on", listenAddr, "for backend:", targetHost)

  reverseProxy := httputil.NewSingleHostReverseProxy(targetURL)

  disallowedHeaders := map[string]bool{
    "x-forwarded-for": true,
    "x-forwarded-host": true,
    "x-forwarded-server": true,
    "x-forwarded-proto": true,
  }

  http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
    for key := range r.Header {
      if strings.HasPrefix(key, ":") || disallowedHeaders[strings.ToLower(key)] {
        r.Header.Del(key)
      }
    }

    r.Header.Set("X-Forwarded-Proto", "https")
    r.Header.Set("X-Forwarded-For", r.RemoteAddr)

    reverseProxy.ServeHTTP(w, r)
  })

  // Use spdy's ListenAndServe. Without NPN.
  if err = spdy.ListenAndServeSPDYNoNPN(listenAddr, certPath, keyPath, nil, 3.1); err != nil {
    // handle error.
    log.Fatal(err)
  }
}
